import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int opcion;
		int n;
		
		System.out.println("INTRODUCE UNA OPCION");
		
		System.out.println("Calcular piramide (1)");
		System.out.println("Calcular factorial (2)");
		System.out.println("Calcular nuemro primo (3)");
		System.out.println("Sacar cadena de nombres (4)");
		opcion=in.nextInt();
		
		switch(opcion) {
		case 1:
			System.out.println("Introduce un numero");
			n=in.nextInt();
			crearPiramide(n);
			break;
		case 2:
			System.out.println("Calcular numero Factorial");
			n=in.nextInt();
			
			
			System.out.println("El factorial es " + calcularFactorial(n));
			break;
			
		case 3:
			System.out.println("Calcular numero primo");
			n=in.nextInt();
			
			boolean valor=calcularPrimo(n);
			if (valor==true)
				System.out.println("Es primo");
			else
				System.out.println("No es primo");
		
			break;
			
		case 4:
			System.out.println("Cantidad de nombres");
			int cantidad=in.nextInt();
			
			String v[]=new String[cantidad];
			
			rellenarCadena(v);
			
			visualizarCadena(v);
			
			break;
		
		
		
		}
	
		
	}

	private static void visualizarCadena(String[] v) {
		for(int i=0; i<v.length; i++){
			System.out.println(v[i]);
		}
		
	}

	private static void rellenarCadena(String[] v) {
		Scanner in=new Scanner(System.in);
		
		for(int i=0; i<v.length; i++){
			System.out.println("Introduce el nombre");
			v[i]=in.nextLine();
		}
		
	}

	private static boolean calcularPrimo(int n) {
		for (int i=2; i<=(n-1); i++){
			if (n%i==0){
				return false;
			}
		}
		return true;
	}

	private static int calcularFactorial(int n) {
		int factorial=1;
		for (int i=1; i<=n; i++){
			factorial=factorial*n;
		}
		return factorial;
	}

	private static void crearPiramide(int n) {
		
	
		for (int linea=1; linea<=n; linea++){
			for(int i=1; i<=linea; i++){
				System.out.print(linea);
			}
			System.out.println();
		}
		
	}

}
