package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Color;
/**
 * 
 * @author Daniel Tom�s
 * @since 12/01/2018
 *
 */
public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/Imagen/touchicon-180.png")));
		setTitle("Aplicacion");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 761, 496);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEditar.add(mntmPegar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmAcercaDe = new JMenuItem("Acerca de..");
		mnAyuda.add(mntmAcercaDe);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.ORANGE);
		panel_1.setForeground(Color.GRAY);
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblCorreoElectronico = new JLabel("Correo Electronico:");
		lblCorreoElectronico.setBounds(67, 94, 117, 14);
		panel_1.add(lblCorreoElectronico);
		
		textField = new JTextField();
		textField.setBounds(275, 91, 183, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setBounds(88, 144, 76, 14);
		panel_1.add(lblContrasea);
		
		textField_1 = new JTextField();
		textField_1.setBounds(275, 141, 183, 20);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnLogIn = new JButton("Log In");
		btnLogIn.setBounds(322, 191, 89, 23);
		panel_1.add(btnLogIn);
		
		JCheckBox chckbxVersion = new JCheckBox("Version");
		chckbxVersion.setBackground(Color.RED);
		chckbxVersion.setBounds(67, 268, 97, 23);
		panel_1.add(chckbxVersion);
		
		JRadioButton radioButton = new JRadioButton("1.1");
		radioButton.setBackground(Color.ORANGE);
		radioButton.setBounds(67, 299, 109, 23);
		panel_1.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("1.2");
		radioButton_1.setBackground(Color.ORANGE);
		radioButton_1.setBounds(67, 320, 109, 23);
		panel_1.add(radioButton_1);
		
		JSlider slider = new JSlider();
		slider.setBounds(512, 296, 200, 26);
		panel_1.add(slider);
		
		JLabel lblVolumen = new JLabel("VOLUMEN");
		lblVolumen.setBounds(439, 297, 76, 26);
		panel_1.add(lblVolumen);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1515538800000L), null, null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(203, 300, 156, 20);
		panel_1.add(spinner);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/Imagen/touchicon-180.png")));
		lblNewLabel.setBounds(529, 11, 183, 203);
		panel_1.add(lblNewLabel);
	}
}
