package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucciÃ³n y avanzar
		 * instrucciÃ³n a instrucciÃ³n (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*
		 * 	La línea verde con la variable charAt.
		 	Hay que modificar la condición “ i<=cadenaLeida.length(); ” 
 			porque la cadena solo puede coger 4 caracteres de 
 			la palabra Hola empezando en 0--H, 1--o… cuando quiere coger el 4 no puede 
 			porque no hay y por eso se le quita el = .

		 */
		for(int i = 0 ; i <= cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
